
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;

import static javax.swing.SwingUtilities.invokeLater;

import Parts.AddEmployee;
import Parts.ListEmployee;


import service.Personnel;

public class Fenetre{

    // Variables pour les instances des différentes pages
    private JPanel ajoutPannel;
    private JPanel listPannel;

    // Variables globales pour les différentes pages
    private JFrame frame;

    //Panel principal
    private JPanel mainPannel = new JPanel();

    Personnel p = new Personnel();


    public Fenetre(JFrame frame) {
        this.frame = frame;

    }

    public static void main(String[] args) {
        invokeLater(Fenetre::window);
    }

    private static void window() {
        Fenetre app = new Fenetre(new JFrame("DashBoard"));
        app.run( new AddEmployee(),  new ListEmployee());  // instantiation des differentes pages
    }



    private void run(AddEmployee ajout, ListEmployee list){
        ajout.run();
        list.run();
        ajout.model = list.model;



        // Définition des tailles boutons
        Dimension d = new Dimension(250,100);


        // Créations des buttons du menu
        JButton addButton = new JButton("Ajout");
        JButton listButton = new JButton("Afficher");

        addButton.setPreferredSize(d);
        listButton.setPreferredSize(d);

        addButton.addActionListener(e -> {
            System.out.println("Ajout");




            ajout.actualize(this.p);

            this.ajoutPannel = ajout.panel;





            this.frame.getContentPane().removeAll();
            this.frame.getContentPane().invalidate();
            this.frame.getContentPane().add(this.ajoutPannel);
            ajout.reset(); // Remise a zero du formulaire
            this.frame.revalidate();
            this.frame.repaint();
        });

        listButton.addActionListener((ActionEvent e) -> {
            System.out.println("Afficher");


            this.frame.getContentPane().removeAll();


            this.listPannel = list.pannel;

            this.frame.getContentPane().add(this.listPannel);

            list.setAverage(String.format(
                    "Le salaire moyen dans l'entreprise est de %.2f euros.",
                    p.salaireMoyen()
            ));

            this.frame.revalidate();
            this.frame.repaint();
        });


        // Définition du panel comme grille pour pouvoir afficher les boutons au centre
        mainPannel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();


        // Ajout des boutons et d'un espace
        mainPannel.add(addButton, gbc);
        mainPannel.add(Box.createRigidArea(new Dimension(10,0)), gbc);
        mainPannel.add(listButton, gbc);


        // Définition de la frame
        this.frame.setSize(720, 320);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setVisible(true);


        // Affichage du menu au démarage

        this.frame.getContentPane().removeAll();
        this.frame.getContentPane().invalidate();
        this.frame.getContentPane().add(this.mainPannel);
        this.frame.revalidate();
        this.frame.repaint();




        // Partage de la frame à ajout et list
        ajout.frame = this.frame;
        ajout.mainPannel = this.mainPannel;

        list.frame = this.frame;
        list.mainPannel = this.mainPannel;

        // Partage du tableau de valeur
        list.model = ajout.model;
    }


}
