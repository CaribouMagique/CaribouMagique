package Parts;

import domain.*;
import service.Personnel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;


public class AddEmployee {

    // Variables pour le button retour
    public JPanel mainPannel;
    public JFrame frame;

    // Panel a exporter
    public JPanel panel = new JPanel();

    // Tableau pour les valeurs
    public DefaultTableModel model;

    // Variables des inputs pour le reset
    private JTextField fNameEntry;
    private JTextField lNameEntry;
    private JTextField ageEntry;
    private JTextField dateEntry;
    private JTextField baseEntry;
    private JComboBox typeCombo;
    private Personnel p;

    public void run() {

        panel.setLayout(null);

        JLabel fNameLabel = new JLabel("Prénom");
        fNameLabel.setBounds(20,10, 200,40);
        panel.add(fNameLabel);

        this.fNameEntry = new JTextField(20);
        this.fNameEntry.setBounds(120, 10, 200, 40);
        panel.add( this.fNameEntry);

        JLabel lNameLabel = new JLabel("Nom");
        lNameLabel.setBounds(20,50, 200,40);
        panel.add(lNameLabel);

        this.lNameEntry = new JTextField(20);
        this.lNameEntry.setBounds(120, 50, 200, 40);
        panel.add( this.lNameEntry);

        JLabel ageLabel = new JLabel("Âge");
        ageLabel.setBounds(20,95, 100,20);
        panel.add(ageLabel);

        this.ageEntry = new JTextField(20);
        this.ageEntry.setBounds(120, 95, 100, 20);
        panel.add( this.ageEntry);

        JLabel dateLabel = new JLabel("Date d'entrée");
        dateLabel.setBounds(20,120, 100,20);
        panel.add(dateLabel);

        this.dateEntry = new JTextField(20);
        this.dateEntry.setBounds(120, 120, 100, 20);
        panel.add( this.dateEntry);


        JLabel baseLabel = new JLabel("Base de calcul");
        baseLabel.setBounds(20,145, 100,20);
        panel.add(baseLabel);

        this.baseEntry = new JTextField(20);
        this.baseEntry.setBounds(120, 145, 100, 20);
        panel.add( this.baseEntry);


        JLabel typeLabel = new JLabel("Type d'employé");
        typeLabel.setBounds(20,170, 100,20);
        panel.add(typeLabel);

        String[] typeList = {"Vendeur", "Technicien", "Technicien à Risque", "Représentant", "Manutentionnaire", "Manutentionnaire à Risque"};
        this.typeCombo = new JComboBox(typeList);
        this.typeCombo.setBounds(120, 170, 200,20);
        panel.add( this.typeCombo);








        // Bouton pour envoyer le formulaire

        JButton submitButton = new JButton("Soumettre");
        submitButton.setBounds(200, 220, 100, 25);
        submitButton.addActionListener(e->{
            String[] newUser = { fNameEntry.getText(), lNameEntry.getText(), ageEntry.getText(), dateEntry.getText(), baseEntry.getText(), typeList[typeCombo.getSelectedIndex()] };
            this.model.addRow(newUser);


            // Le switch redirige la creation de l'employee en fonction de la combo box
            switch (typeCombo.getSelectedIndex()){
                case 0 :
                    p.ajouterEmploye(new Vendeur(fNameEntry.getText(), lNameEntry.getText(), Integer.parseInt(ageEntry.getText()) , dateEntry.getText(), Integer.parseInt(baseEntry.getText())));
                    break;
                case 1 :
                    p.ajouterEmploye(new Technicien(fNameEntry.getText(), lNameEntry.getText(), Integer.parseInt(ageEntry.getText()) , dateEntry.getText(), Integer.parseInt(baseEntry.getText())));
                    break;
                case 2 :
                    p.ajouterEmploye(new TechnARisque(fNameEntry.getText(), lNameEntry.getText(), Integer.parseInt(ageEntry.getText()) , dateEntry.getText(), Integer.parseInt(baseEntry.getText())));
                    break;
                case 3 :
                    p.ajouterEmploye(new Representant(fNameEntry.getText(), lNameEntry.getText(), Integer.parseInt(ageEntry.getText()) , dateEntry.getText(), Integer.parseInt(baseEntry.getText())));
                    break;
                case 4 :
                    p.ajouterEmploye(new Manutentionnaire(fNameEntry.getText(), lNameEntry.getText(), Integer.parseInt(ageEntry.getText()) , dateEntry.getText(), Integer.parseInt(baseEntry.getText())));
                    break;
                case 5 :
                    p.ajouterEmploye(new ManutARisque(fNameEntry.getText(), lNameEntry.getText(), Integer.parseInt(ageEntry.getText()) , dateEntry.getText(), Integer.parseInt(baseEntry.getText())));
                    break;
            }


            System.out.println("Submit member");
            this.reset();
        });
        panel.add(submitButton);


        // Bouton pour retourner au menu

        JButton backButton = new JButton("Retour");
        backButton.setBounds(90, 220, 100, 25);

        backButton.addActionListener(e -> {
            System.out.println("Ajout");
            this.frame.getContentPane().removeAll();
            this.frame.getContentPane().invalidate();
            this.frame.getContentPane().add(this.mainPannel);
            this.frame.revalidate();
            this.frame.repaint();
        });

        this.panel.add(backButton);

    }

    // fonction pour remettre le formulaire à zero
    public void reset() {
        this.fNameEntry.setText("");
        this.lNameEntry.setText("");
        this.ageEntry.setText("");
        this.dateEntry.setText("");
        this.baseEntry.setText("");
        this.typeCombo.setSelectedIndex(0);
    }

    public void actualize(Personnel perso) {
        this.p = perso;
    }
}
