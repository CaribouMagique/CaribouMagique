package Parts;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;


public class ListEmployee {

    // Variables pour le bouton retour
    public JPanel mainPannel;
    public JFrame frame;

    // Panel à exporter
    public JPanel pannel = new JPanel();

    // Variable contenant le jLabel pour pouvoir le metre a jour
    public JLabel average = new JLabel();

    // Tableau pour les valeurs
    public DefaultTableModel model = new DefaultTableModel();

    public void run() {

        // Bouton pour retourner au menu

        JButton RetourToMenu = new JButton("Retour");

        RetourToMenu.addActionListener(e -> {
            this.frame.getContentPane().removeAll();
            this.frame.getContentPane().add(this.mainPannel);
            this.frame.revalidate();
            this.frame.repaint();
        });

        this.pannel.add(RetourToMenu);


        // Cération des colones
        model.addColumn("First Name");
        model.addColumn("Last Name");
        model.addColumn("Age");
        model.addColumn("Entry Date");
        model.addColumn("Base de calcul");
        model.addColumn("Type");


        // Ajout du label de la moyenne
        this.pannel.add(this.average);

        // Affichage du tableau et du scroll panel

        JTable table = new JTable(this.model);
        table.setPreferredScrollableViewportSize(new Dimension(680, 300));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        this.pannel.add(scrollPane);




    }
    // Foncton pour mettre a jour la moyenne
    public void setAverage(String text){
        // Mise a jour du texte
        this.average.setText(text);
        // Affichage du label
        this.average.paintImmediately(this.average.getVisibleRect());
    }
}
